﻿
using System;

namespace LendFoundry.Tasks.Syndication.SyndicationCalled
{
    public static class Settings
    {
        public const string Prefix = "TASK_SYNDICATION_CALLED";
        public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "task_syndication_called";
        public static string Tenant { get; } = Environment.GetEnvironmentVariable("TASK_TENANT") ?? "my-tenant";
    }
}