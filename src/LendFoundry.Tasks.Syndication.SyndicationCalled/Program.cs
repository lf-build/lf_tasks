﻿using LendFoundry.EventHub.Client;
using LendFoundry.Configuration.Client;
using LendFoundry.DocumentGenerator.Client;
using LendFoundry.DocumentManager.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.Tasks.Syndication.SyndicationCalled.Configuration;
using LendFoundry.Foundation.ServiceDependencyResolver;
using LendFoundry.Tenant.Client;
#if DOTNET2
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

#else
using Microsoft.AspNet.Http;
using Microsoft.Framework.DependencyInjection;
#endif
using System;

namespace LendFoundry.Tasks.Syndication.SyndicationCalled
{
    public class Program : DependencyInjection
    {
        public static void Main()
        {
            Console.WriteLine("Application started");

            Program p = new Program();
            p.Provider.GetService<IAgent>().Execute();
            Console.WriteLine("Application terminated");
        }


        protected override IServiceCollection ConfigureServices(IServiceCollection services)
        {
            services.AddTokenHandler();
            services.AddTransient<IHttpContextAccessor, EmptyHttpContextAccessor>();
            services.AddEventHub(Settings.ServiceName);
            services.AddTenantService();
            services.AddConfigurationService<SyndicationCalledConfiguration>(Settings.ServiceName);
            services.AddDependencyServiceUriResolver<SyndicationCalledConfiguration>(Settings.ServiceName);         
            services.AddDocumentGenerator();
            services.AddDocumentManager();
            services.AddServiceLogging(Settings.ServiceName, NullLogContext.Instance);
            services.AddTransient<IAgent, SyndicationCalledAgent>();
            return services;
        }
    }
}