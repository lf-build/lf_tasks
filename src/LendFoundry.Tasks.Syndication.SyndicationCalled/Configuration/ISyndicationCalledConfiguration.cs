﻿using System.Collections.Generic;
using LendFoundry.Foundation.Client;

namespace LendFoundry.Tasks.Syndication.SyndicationCalled.Configuration
{
    public interface ISyndicationCalledConfiguration: IDependencyConfiguration
    {
        Dictionary<string, List<SyndicationSubscription>> Syndications { get; set; }
    }
}