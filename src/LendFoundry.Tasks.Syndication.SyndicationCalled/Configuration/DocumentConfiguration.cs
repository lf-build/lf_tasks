using System.Collections.Generic;

namespace LendFoundry.Tasks.Syndication.SyndicationCalled.Configuration
{
    public class DocumentConfiguration
    {
        public string Name { get; set; }
        public List<string> Tags { get; set; }
    }
}