namespace LendFoundry.Tasks.Syndication.SyndicationCalled.Configuration
{
    public class SyndicationSubscription
    {
        public string SyndicationName { get; set; }

        public string TemplateName { get; set; }

        public string TemplateVersion { get; set; }

        public DocumentConfiguration Document { get; set; }
    }
}