﻿using System.Collections.Generic;

namespace LendFoundry.Tasks.Syndication.SyndicationCalled.Configuration
{
    public class SyndicationCalledConfiguration : ISyndicationCalledConfiguration
    {
        public Dictionary<string, List<SyndicationSubscription>> Syndications { get; set; }
        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }
        public string ConnectionString { get; set; }
    }
}