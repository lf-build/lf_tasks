﻿using LendFoundry.Configuration;
using LendFoundry.DocumentGenerator;
using LendFoundry.DocumentManager.Client;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Client;
using LendFoundry.Security.Tokens;
using LendFoundry.SyndicationStore.Events;
using LendFoundry.Tasks.Agent;
using LendFoundry.Tasks.Syndication.SyndicationCalled.Configuration;
using LendFoundry.TemplateManager;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using IDocumentGeneratorServiceFactory = LendFoundry.DocumentGenerator.Client.IDocumentGeneratorServiceFactory;
using LendFoundry.Tenant.Client;

namespace LendFoundry.Tasks.Syndication.SyndicationCalled
{
    public class SyndicationCalledAgent : EventBasedAgent
    {
        private IConfigurationServiceFactory ConfigurationFactory { get; }
        private IDocumentGeneratorServiceFactory DocumentGeneratorFactory { get; }
        private IDocumentManagerServiceFactory DocumentManagerFactory { get; }

        public SyndicationCalledAgent
        (
            ITokenHandler tokenHandler,
            IConfigurationServiceFactory configurationFactory,
            IEventHubClientFactory eventHubFactory,
            ILoggerFactory loggerFactory,
            IDocumentGeneratorServiceFactory documentGeneratorFactory,
            IDocumentManagerServiceFactory documentManagerFactory,
            ITenantServiceFactory tenantServiceFactory
        ) : base(eventHubFactory, tenantServiceFactory, configurationFactory, Settings.ServiceName, nameof(SyndicationCalledEvent), tokenHandler, loggerFactory)
        {
            ConfigurationFactory = configurationFactory;
            DocumentGeneratorFactory = documentGeneratorFactory;
            DocumentManagerFactory = documentManagerFactory;
        }

        public override async void Execute(EventInfo @event)
        {
            var logger = LoggerFactory.Create(NullLogContext.Instance);
            var token = TokenHandler.Issue(Settings.Tenant, Settings.ServiceName);
            var reader = new StaticTokenReader(token.Value);
            var configurationService = ConfigurationFactory.Create<SyndicationCalledConfiguration>(Settings.ServiceName, reader);
            var configuration = configurationService.Get();
            if (configuration?.Syndications == null)
            {
                logger.Warn($"No configuration found for {Settings.ServiceName} task");
                return;
            }

            var syndicationEvent = @event.Cast<SyndicationCalledEvent>();
            if (syndicationEvent == null)
            {
                logger.Error($"Cast from {nameof(SyndicationBasedEvent)} cannot is possible");
                return;
            }

            var syndicationKeyValue = configuration.Syndications.FirstOrDefault(syn =>
                string.Equals(syn.Key, syndicationEvent.EntityType, StringComparison.CurrentCultureIgnoreCase));

            var subscription = GetSubscription(syndicationKeyValue, logger, syndicationEvent);
            if (subscription == null)
                return;

            try
            {
                var documentGenerator = DocumentGeneratorFactory.Create(reader);
                var documentManager = DocumentManagerFactory.Create(reader);
                var document = new Document
                {
                    Data = @event.Data,
                    Name = subscription.Document.Name,
                    Version = subscription.TemplateVersion,
                    Format = DocumentFormat.Pdf
                };

                var documentResult = await documentGenerator.Generate(subscription.TemplateName, subscription.TemplateVersion, Format.Html, document);
                documentResult.DocumentName = documentResult.DocumentName;

                using (var stream = new MemoryStream(documentResult.Content))
                {
                    var documentInfo = await documentManager.Create
                    (
                        stream,
                        syndicationEvent.EntityType,
                        syndicationEvent.EntityId,
                        documentResult.DocumentName,
                        subscription.Document.Tags
                    );

                    await EventHubFactory
                        .Create(reader)
                        .Publish("TaskSyndicationCalled", new { Document = documentInfo })
                        .ContinueWith(t =>
                        {
                            logger.Info("Task-syndication-called generated and stored document");
                        });
                }
            }
            catch (ClientException ex)
            {
                logger.Error($"A client exception occured in {Settings.ServiceName} with message: {ex.Message}");
            }
            catch (Exception ex)
            {
                logger.Error($"An exception occured in {Settings.ServiceName} with message: {ex.Message}");
            }
        }

        private static SyndicationSubscription GetSubscription
        (
            KeyValuePair<string, List<SyndicationSubscription>> syndicationKeyValue,
            ILogger logger,
            SyndicationCalledEvent syndicationEvent
        )
        {
            var syndicationKey = syndicationKeyValue.Key;

            var syndicationSubscription = syndicationKeyValue.Value?.FirstOrDefault(entity =>
                string.Equals(entity.SyndicationName, syndicationEvent.Name, StringComparison.CurrentCultureIgnoreCase));

            if (!IsConfigurationValid(syndicationKey, logger, syndicationEvent, syndicationSubscription))
                return null;

            return syndicationSubscription;
        }

        private static bool IsConfigurationValid
        (
            string syndicationKey,
            ILogger logger,
            SyndicationCalledEvent syndicationEvent,
            SyndicationSubscription syndicationSubscription
        )
        {
            if (string.IsNullOrWhiteSpace(syndicationKey) || syndicationSubscription == null)
            {
                logger.Warn($"Syndication subscription is null event raised by {syndicationEvent.Name}");
                return false;
            }

            if (string.IsNullOrWhiteSpace(syndicationSubscription.SyndicationName))
            {
                logger.Warn($"Syndication name is null or empty for {syndicationKey} event raised by {syndicationEvent.Name}");
                return false;
            }

            if (string.IsNullOrWhiteSpace(syndicationSubscription.TemplateName))
            {
                logger.Warn($"Template is null or empty for {syndicationKey} event raised by {syndicationEvent.Name}");
                return false;
            }

            if (string.IsNullOrWhiteSpace(syndicationSubscription.TemplateVersion))
            {
                logger.Warn($"Template version is null or empty for {syndicationKey} event raised by {syndicationEvent.Name}");
                return false;
            }

            if (syndicationSubscription.Document == null)
            {
                logger.Warn($"Document is null for {syndicationKey} event raised by {syndicationEvent.Name}");
                return false;
            }

            if (string.IsNullOrWhiteSpace(syndicationSubscription.Document.Name))
            {
                logger.Warn($"Document name is null for {syndicationKey} event raised by {syndicationEvent.Name}");
                return false;
            }

            return true;
        }
    }
}