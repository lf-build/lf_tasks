using LendFoundry.Calendar.Client;
using LendFoundry.Configuration;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.MoneyMovement.Ach.Client;
using LendFoundry.Security.Tokens;
using LendFoundry.Tasks.Agent;
using LendFoundry.Tenant.Client;

namespace LendFoundry.Tasks.Ach.ProcessAchFile
{
    public class Agent : ScheduledAgent
    {
        public Agent
        (
            IConfigurationServiceFactory configurationFactory, 
            ITokenHandler tokenHandler, 
            ITenantTimeFactory tenantTimeFactory, 
            ICalendarServiceFactory calendarServiceFactory, 
            IAchServiceFactory achServiceFactory, 
            ILogger logger,
            ILoggerFactory loggerFactory,
            ITenantServiceFactory tenantServiceFactory
        )
            : base(tokenHandler, configurationFactory, tenantTimeFactory, tenantServiceFactory, loggerFactory, Settings.ServiceName)
        {
            ConfigurationFactory = configurationFactory;
            TokenHandler = tokenHandler;
            TenantTimeFactory = tenantTimeFactory;
            CalendarServiceFactory = calendarServiceFactory;
            AchServiceFactory = achServiceFactory;
            Logger = logger;
            TenantServiceFactory = tenantServiceFactory;
        }

        private ITenantServiceFactory TenantServiceFactory { get; }

        private ISettingReader SettingReader { get; }

        private IConfigurationServiceFactory ConfigurationFactory { get; }

        private ITenantTimeFactory TenantTimeFactory { get; }

        private ITokenHandler TokenHandler { get; }

        private ICalendarServiceFactory CalendarServiceFactory { get; }

        private IAchServiceFactory AchServiceFactory { get; }

        private ILogger Logger { get; }

        public override void OnSchedule(string tenant)
        {
            var token = TokenHandler.Issue(tenant, Settings.ServiceName);
            var reader = new StaticTokenReader(token.Value);
            var tenantTime = TenantTimeFactory.Create(ConfigurationFactory, reader);
            var calendar = CalendarServiceFactory.Create(reader);
            var ach = AchServiceFactory.Create(reader);

            if (!IsBusinessDay(calendar, tenantTime))
            {
                Logger.Info("Not a business day");
                return;
            }

            Logger.Info("Processing ACH file");
            ach.ProcessReturnFile();
        }

        private static bool IsBusinessDay(ICalendarService calendar, ITenantTime tenantTime)
        {
            var today = tenantTime.Today;
            var todayInfo = calendar.GetDate(today.Year, today.Month, today.Day);
            return todayInfo.Type == DateType.Business;
        }
    }
}
