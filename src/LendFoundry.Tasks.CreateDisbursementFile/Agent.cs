using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using LendFoundry.Calendar.Client;
using LendFoundry.Configuration;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.MoneyMovement.Ach.Client;
using LendFoundry.MoneyMovement.Ach.Configuration;
using LendFoundry.Security.Tokens;
using LendFoundry.Tasks.Agent;
using LendFoundry.Tenant.Client;
using LMS.Loan.Filters.Client;
using LMS.LoanAccounting;

namespace LendFoundry.Tasks.CreateDisbursementFile
{
    public class Agent : ScheduledAgent
    {
        public Agent
        (
            IConfigurationServiceFactory configurationFactory, 
            ITokenHandler tokenHandler,
            ITenantTimeFactory tenantTimeFactory, 
            ICalendarServiceFactory calendarServiceFactory, 
            ILoggerFactory loggerFactory,
            ITenantServiceFactory tenantServiceFactory,
            ILoanFilterClientServiceFactory loanFilterServiceFactory,
            IFileStorageFactory fileStorageFactory,
            ICsvGenerator csvGenerator,
            IAccrualBalanceClientFactory accrualBalanceClientFactory,
            IAchConfigurationServiceFactory achConfigurationServiceFactory
            )
            : base(tokenHandler, configurationFactory, tenantTimeFactory, tenantServiceFactory, loggerFactory, Settings.ServiceName)
        {
            ConfigurationFactory = configurationFactory;
            TokenHandler = tokenHandler;
            TenantTimeFactory = tenantTimeFactory;
            CalendarServiceFactory = calendarServiceFactory;
            LoggerFactory = loggerFactory;
            LoanFilterServiceFactory = loanFilterServiceFactory;
            FileStorageFactory = fileStorageFactory;
            CsvGenerator = csvGenerator;
            AccrualBalanceClientFactory = accrualBalanceClientFactory;
            AchConfigurationServiceFactory = achConfigurationServiceFactory;
        }

        private ILoanFilterClientServiceFactory LoanFilterServiceFactory { get; }
        private IFileStorageFactory FileStorageFactory { get; }
        private ICsvGenerator CsvGenerator { get; }
        private IAccrualBalanceClientFactory AccrualBalanceClientFactory { get; }
        private IConfigurationServiceFactory ConfigurationFactory { get; }

        private ITenantTimeFactory TenantTimeFactory { get; }

        private ITokenHandler TokenHandler { get; }

        private ICalendarServiceFactory CalendarServiceFactory { get; }
        private IAchConfigurationServiceFactory AchConfigurationServiceFactory { get; }

        private ILoggerFactory LoggerFactory { get; }

        public override void OnSchedule(string tenant)
        {
            var token = TokenHandler.Issue(tenant, Settings.ServiceName);
            var reader = new StaticTokenReader(token.Value);
            var logger = LoggerFactory.Create(NullLogContext.Instance);

            var tenantTime = TenantTimeFactory.Create(ConfigurationFactory, reader);
            var calendar = CalendarServiceFactory.Create(reader);
            var achService = AchConfigurationServiceFactory.Create(reader);

            if (!IsBusinessDay(calendar, tenantTime))
            {
                logger.Warn("Not a business day");
                return;
            }

            var configurationServiceFactory = ConfigurationFactory.Create<Configuration>(Settings.ServiceName, reader);
            if (configurationServiceFactory == null)
            {
                logger.Error($"Was not found the Configuration related to {Settings.ServiceName} Task");
                return;
            }

            var configuration = configurationServiceFactory.Get();
            if (configuration == null)
            {
                logger.Error($"Was not found the Configuration related to {Settings.ServiceName} Task");
                return;
            }
            

            var loanService = LoanFilterServiceFactory.Create(reader);
            var loanAccountingService = AccrualBalanceClientFactory.Create(reader);
            //TODO: Create new method to get all loan onboarded today
            var loans = loanService.GetLoansByOnBoardedByDate(tenantTime.Now.Date.Year,tenantTime.Now.Date.Month,tenantTime.Now.Date.Day).Result;
            var disbursementRecords = new List<DisbursementRecord>();
            foreach (var loan in loans)
            {
                FundingSource funderDetails = null;
                if(!string.IsNullOrWhiteSpace(loan.FunderId))
                {
                    funderDetails = achService.GetFundingSourceById(loan.FunderId);
                }
                var loanFees = loanAccountingService.GetFees(loan.LoanNumber).Result;
                disbursementRecords.Add(new DisbursementRecord(loan,loanFees, funderDetails != null ? funderDetails.Name : string.Empty));
            }

            if (disbursementRecords.Any())
            {
                try
                {
                    var fileStorageService = FileStorageFactory.Create(configuration.FileStorage, logger);

                    using (var csvMemoryStream = new MemoryStream(CsvGenerator.Write(disbursementRecords, configuration.CsvProjection)))
                    {
                        var fileName = $"Disbursement_{tenantTime.Now:yyyy-MM-dd-hh-mm-ss}.csv";
                        fileStorageService.Upload(csvMemoryStream, fileName, configuration.FileStorage.Path);
                        logger.Info("Boarding files uploaded successfully");
                    }
                }
                catch (Exception ex)
                {
                    logger.Error($"Error while uploading boarding file to sftp: ", ex);
                    return;
                }
            }
            
            logger.Info("Creating disbursement file");

        }

        private static bool IsBusinessDay(ICalendarService calendar, ITenantTime tenantTime)
        {
            var today = tenantTime.Today;
            var todayInfo = calendar.GetDate(today.Year, today.Month, today.Day);
            return todayInfo.Type == DateType.Business;
        }
    }
}