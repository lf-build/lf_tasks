using System.Collections.Generic;
using LendFoundry.Foundation.Client;

namespace LendFoundry.Tasks.CreateDisbursementFile
{
    public class Configuration : IDependencyConfiguration
    {
        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }
        public FileStorage FileStorage { get; set; }
        public Dictionary<string, string> CsvProjection { get; set; }        

    }
}