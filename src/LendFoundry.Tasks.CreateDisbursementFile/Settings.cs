using System;

namespace LendFoundry.Tasks.CreateDisbursementFile
{
    public static class Settings
    {
        public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "task_create_disbursement_files";
    }
}