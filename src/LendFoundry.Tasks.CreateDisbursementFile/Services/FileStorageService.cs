using System;
using System.Collections.Generic;
using System.IO;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Tasks.CreateDisbursementFile;
using Renci.SshNet;

namespace LendFoundry.Tasks.Applications.CreateOnBoardingFile
{
    public class FileStorageService : IFileStorageService
    {
        public FileStorageService
        (
            FileStorage fileStorage,
            ILogger logger
        )
        {
            if (fileStorage == null) throw new ArgumentException($"{nameof(fileStorage)} is mandatory");
            if (logger == null) throw new ArgumentException($"{nameof(logger)} is mandatory");

            if (string.IsNullOrWhiteSpace(fileStorage.Host))
                throw new ArgumentException($"{nameof(fileStorage.Host)} is mandatory");
            if (fileStorage.Port < 0)
                throw new ArgumentException($"{nameof(fileStorage.Port)} is mandatory");
            if (string.IsNullOrWhiteSpace(fileStorage.Username))
                throw new ArgumentException($"{nameof(fileStorage.Username)} is mandatory");
            if (string.IsNullOrWhiteSpace(fileStorage.Password))
                throw new ArgumentException($"{nameof(fileStorage.Password)} is mandatory");
            if (string.IsNullOrWhiteSpace(fileStorage.Path))
                throw new ArgumentException($"{nameof(fileStorage.Path)} is mandatory");
            Logger = logger;
            FileStorage = fileStorage;

        }

        private ILogger Logger { get; set; }
        private FileStorage FileStorage { get; set; }

        private SftpClient GetSftpClient()
        {
            return new SftpClient(FileStorage.Host, FileStorage.Port, FileStorage.Username, FileStorage.Password);
        }

        public List<string> DownloadFiles(string sourcePath)
        {
            if (string.IsNullOrWhiteSpace(sourcePath))
                throw new ArgumentException("Argument is null or whitespace", nameof(sourcePath));

            var fileNameList = new List<string>();
            var inboxLocation = sourcePath;

            using (var client = GetSftpClient())
            {
                FaultRetry.RunWithAlwaysRetry(client.Connect);

                foreach (var file in client.ListDirectory(inboxLocation))
                {
                    if (file.IsRegularFile)
                    {
                        try
                        {
                            FaultRetry.RunWithAlwaysRetry(
                                () =>
                                {
                                    using (var fs = new FileStream(FileStorage.Path + file.Name, FileMode.Create))
                                    {
                                        client.DownloadFile(file.FullName, fs);
                                        fs.Close();
                                    }
                                });

                            fileNameList.Add(file.Name);
                        }
                        catch (Exception exception)
                        {
                            Logger.Error("Unable to download file", exception);
                            // ignored
                        }
                    }
                }
            }

            return fileNameList;
        }

        public bool IsExist(string fileName)
        {
            if (string.IsNullOrWhiteSpace(fileName))
                throw new ArgumentException("Argument is null or whitespace", nameof(fileName));

            using (var client = GetSftpClient())
            {
                FaultRetry.RunWithAlwaysRetry(client.Connect);

                return client.Exists(FileStorage.Path + fileName);
            }
        }

        public void Delete(string fileName)
        {
            if (string.IsNullOrWhiteSpace(fileName))
                throw new ArgumentException("Argument is null or whitespace", nameof(fileName));

            using (var client = GetSftpClient())
            {
                FaultRetry.RunWithAlwaysRetry(client.Connect);

                client.DeleteFile(FileStorage.Path + fileName);
            }
        }

        public void Upload(string fileFullName)
        {
            if (string.IsNullOrWhiteSpace(fileFullName))
                throw new ArgumentException("Argument is null or whitespace", nameof(fileFullName));

            using (var client = GetSftpClient())
            {
                if (client == null)
                {
                    throw new InvalidOperationException("Sftp Client is not initialized");
                }

                FaultRetry.RunWithAlwaysRetry(client.Connect);
                var destination = $"{FileStorage.Path}/{ Path.GetFileName(fileFullName)}";

                FaultRetry.RunWithAlwaysRetry(
                    () => client.UploadFile(File.OpenRead(fileFullName), destination, true));
            }
        }

        public void Upload(Stream stream, string fileName)
        {
            if (string.IsNullOrWhiteSpace(fileName))
                throw new ArgumentException($"{nameof(fileName)} is mandatory");


            using (var client = GetSftpClient())
            {
                if (client == null)
                {
                    throw new InvalidOperationException("Sftp Client is not initialized");
                }

                FaultRetry.RunWithAlwaysRetry(client.Connect);
                var destination = $"{FileStorage.Path}/{ fileName}";
                stream.Position = 0; // force to use the first position
                FaultRetry.RunWithAlwaysRetry(() =>
                {
                    client.UploadFile(stream, destination, true, (e) => { });
                });
            }
        }

        public void Upload(Stream stream, string fileName, string path)
        {
            if (string.IsNullOrWhiteSpace(fileName))
                throw new ArgumentException($"{nameof(fileName)} is mandatory");


            using (var client = GetSftpClient())
            {
                if (client == null)
                {
                    throw new InvalidOperationException("Sftp Client is not initialized");
                }

                FaultRetry.RunWithAlwaysRetry(client.Connect);
                var destination = $"{path}/{ fileName}";
                stream.Position = 0; // force to use the first position
                FaultRetry.RunWithAlwaysRetry(() =>
                {
                    client.UploadFile(stream, destination, true, (e) => { });
                });
            }
        }

        public bool Exist(string path)
        {
            if (string.IsNullOrWhiteSpace(path))
                throw new ArgumentException($"{nameof(path)} is mandatory");

            using (var client = GetSftpClient())
            {
                FaultRetry.RunWithAlwaysRetry(client.Connect);

                return client.Exists(FileStorage.Path + path);
            }
        }

        public void CreateDirectory(string path)
        {
            if (string.IsNullOrWhiteSpace(path))
                throw new ArgumentException($"{nameof(path)} is mandatory");

            using (var client = GetSftpClient())
            {
                FaultRetry.RunWithAlwaysRetry(client.Connect);

                client.CreateDirectoryRecursively(path);
            }
        }
    }
}