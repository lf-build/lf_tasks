using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using CsvHelper;
using CsvHelper.Configuration;

namespace LendFoundry.Tasks.CreateDisbursementFile
{
    public class CsvGenerator : ICsvGenerator
    {
        private Dictionary<string, string> CsvProjection { get; set; }

        public byte[] Write<T>(IEnumerable<T> items, Dictionary<string, string> csvProjection)
        {
            CsvProjection = csvProjection;
            using (var memoryStream = new MemoryStream())
            {
                using (var streamWriter = new StreamWriter(memoryStream))
                using (var csvWriter = new CsvWriter(streamWriter, SetCsvProjectionValues()))
                {
                    csvWriter.WriteRecords(items);
                } // StreamWriter gets flushed here.

                return memoryStream.ToArray();
            }
        }

        private CsvConfiguration SetCsvProjectionValues()
        {
            var csvConfiguration = new CsvConfiguration()
            {
                Encoding = Encoding.UTF8,
            };

            if (CsvProjection != null && CsvProjection.Any())
                csvConfiguration.RegisterClassMap(SetCsvClassMap());
            return csvConfiguration;
        }

        private CsvClassMap SetCsvClassMap()
        {
            var boardingFileMap = new DefaultCsvClassMap<DisbursementRecord>();
            Dictionary<string, string> csvProjection = CsvProjection;
            var columnIndex = 0;
            foreach (var csvKeyColumn in csvProjection)
            {
                var schemaPropertyName = csvKeyColumn.Key;
                var csvColumnName = csvKeyColumn.Value;
                if (!String.IsNullOrEmpty(csvColumnName))
                {
                    var propertyInfo = typeof(DisbursementRecord).GetProperty(schemaPropertyName);
                    var newMap = new CsvPropertyMap(propertyInfo);
                    newMap.Name(csvColumnName);
                    newMap.Index(columnIndex);
                    boardingFileMap.PropertyMaps.Add(newMap);
                    columnIndex += 1;
                }
            }

            return boardingFileMap;
        }
    }
}