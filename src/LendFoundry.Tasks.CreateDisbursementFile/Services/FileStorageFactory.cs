using LendFoundry.Foundation.Logging;
using LendFoundry.Tasks.Applications.CreateOnBoardingFile;

namespace LendFoundry.Tasks.CreateDisbursementFile
{
    public class FileStorageFactory : IFileStorageFactory
    {
        public IFileStorageService Create(FileStorage fileStorage, ILogger logger)
        {
            return new FileStorageService(fileStorage, logger);
        }
    }
}