﻿using System;
using LendFoundry.Calendar.Client;
using LendFoundry.Configuration.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.ServiceDependencyResolver;
using LendFoundry.Foundation.Services;
using LendFoundry.MoneyMovement.Ach.Client;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using LMS.Loan.Filters.Client;
using LMS.LoanAccounting.Client;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;


namespace LendFoundry.Tasks.CreateDisbursementFile
{
    public class Program : DependencyInjection
    {
        public static void Main(string[] args)
        {

            Console.WriteLine("Application started");
            Program p = new Program();
            p.Provider.GetService<IAgent>().Execute();
            Console.WriteLine("Application terminated");
           
        }

        protected override IServiceCollection ConfigureServices(IServiceCollection services)
        {
            services.AddTokenHandler();
            services.AddTenantTime();
            services.AddTenantService();
            services.AddServiceLogging(Settings.ServiceName, NullLogContext.Instance);
            services.AddCalendarService();
            services.AddAchConfigurationService();
            services.AddConfigurationService<Configuration>(Settings.ServiceName);
            services.AddDependencyServiceUriResolver<Configuration>(Settings.ServiceName);
            services.AddLoanFilterService();
            services.AddAccrualBalance();
            services.AddTransient<IHttpContextAccessor, EmptyHttpContextAccessor>();
            services.AddTransient<IAgent, Agent>();
            services.AddTransient<ICsvGenerator, CsvGenerator>();
            services.AddTransient<IFileStorageFactory, FileStorageFactory>();

            return services;
        }
    }
}