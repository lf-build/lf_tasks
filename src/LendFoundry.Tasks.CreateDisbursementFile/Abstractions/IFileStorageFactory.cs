using LendFoundry.Foundation.Logging;

namespace LendFoundry.Tasks.CreateDisbursementFile
{
    public interface IFileStorageFactory
    {
        IFileStorageService Create(FileStorage fileStorage, ILogger logger);
    }
}