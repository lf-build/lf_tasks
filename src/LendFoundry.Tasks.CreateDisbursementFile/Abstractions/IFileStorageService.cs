using System.Collections.Generic;
using System.IO;

namespace LendFoundry.Tasks.CreateDisbursementFile
{
    public interface IFileStorageService
    {
        void Upload(string fileFullName);
        void Upload(Stream stream, string fileName);
        List<string> DownloadFiles(string sourcePath);
        bool IsExist(string fileName);
        void Delete(string fileName);
        void Upload(Stream stream, string fileName, string path);
        bool Exist(string path);
        void CreateDirectory(string path);
    }
}