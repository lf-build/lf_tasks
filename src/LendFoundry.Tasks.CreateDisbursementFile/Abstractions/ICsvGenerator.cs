using System.Collections.Generic;

namespace LendFoundry.Tasks.CreateDisbursementFile
{
    public interface ICsvGenerator
    {
        byte[] Write<T>(IEnumerable<T> items, Dictionary<string, string> csvProjection);
    }
}