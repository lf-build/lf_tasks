using System.Collections.Generic;
using System.Linq;
using LMS.Loan.Filters;
using LMS.LoanAccounting;

namespace LendFoundry.Tasks.CreateDisbursementFile
{
    public class DisbursementRecord
    {
        public string Id { get; }
        public string LoanId { get; set; }
        public string FunderName { get; set; }
        public string FullName { get;  }
        public string BusinessName { get; }
        public double LoanAmount { get;  }
        public double AnnualRate { get;  }
        public int Tenure { get;  }
        public string AggregatorName { get; }
        public double ProcessingFees { get;  }
        public string Beneficiary { get; }

        public DisbursementRecord(IFilterView filterView, List<IFee> fees, string funderName)
        {

            FullName = filterView.PrimaryApplicantFirstName + " " + filterView.PrimaryApplicantLastName;
            BusinessName = filterView.PrimaryBusinessName;
            LoanAmount = filterView.LoanAmount;
            AnnualRate = filterView.AnnualRate;
            Tenure = filterView.Tenure;
            AggregatorName = filterView.AggregatorName;
            Beneficiary = filterView.Beneficiary;
            Id = filterView.AdditionalRefId;
            LoanId = filterView.LoanNumber;
            FunderName = funderName;
            if (fees != null)
            {
                ProcessingFees = fees.Where(f => f.IsOnBoard == true).Sum(r => r.FeeAmount);
            }
        }
    }
}