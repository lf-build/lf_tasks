﻿
using System;

namespace LendFoundry.Tasks.DecisionEngine.Event
{
    public static class Settings
    {        
        public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "task-decision-engine-event";
        public static string Tenant { get; } = Environment.GetEnvironmentVariable("TASK_TENANT");
        public static string EventName { get; } = Environment.GetEnvironmentVariable("TASK_EVENT_NAME") ?? "ExecuteRuleRequested";
        public static string FunctionName { get; } = Environment.GetEnvironmentVariable($"TASK_FUNCTION_NAME");
    }
}