﻿using LendFoundry.Clients.DecisionEngine;
using LendFoundry.Configuration;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using LendFoundry.Tasks.Agent;
using LendFoundry.Tenant.Client;
using System;

namespace LendFoundry.Tasks.DecisionEngine.Event
{
    public class Agent : EventBasedAgent
    {
        public Agent
        (
            ITokenHandler tokenHandler,
            ITokenReaderFactory tokenReaderFactory,
            ILoggerFactory loggerFactory,
            IDecisionEngineClientFactory decisionEngineFactory, 
            IEventHubClientFactory factory,
            ITenantServiceFactory tenantServiceFactory,
            IConfigurationServiceFactory configurationServiceFactory
        )
            : base(factory, tenantServiceFactory, configurationServiceFactory, Settings.ServiceName, Settings.EventName, tokenHandler, loggerFactory)
        {
            TokenReaderFactory = tokenReaderFactory;
            DecisionEngineFactory = decisionEngineFactory;
            FunctionName = Settings.FunctionName;
        }
        
        private ITokenReaderFactory TokenReaderFactory { get; }

        private ILogger Logger { get; set; }

        private IDecisionEngineClientFactory DecisionEngineFactory { get; set; }

        private string FunctionName { get; set; }

        public override void Execute(EventInfo @event)
        {
            Logger = LoggerFactory.CreateLogger();
            Logger.Info($"Received event {@event.Name}, executing function {FunctionName}.");
            try
            {
                var token = TokenHandler.Issue(@event.TenantId, Settings.ServiceName);
                var reader = TokenReaderFactory.Create(@event.TenantId, string.Empty);
                var decisionEngine = DecisionEngineFactory.Create(reader);
                var response = decisionEngine.Execute<dynamic>(FunctionName, new
                {
                    payload = @event
                });
                Logger.Info($"Function {FunctionName} executed with following response {response}.");

            }
            catch (Exception ex)
            {
                Logger.Error($"Problems executing function {FunctionName}", ex);
            }
        }
    }
}