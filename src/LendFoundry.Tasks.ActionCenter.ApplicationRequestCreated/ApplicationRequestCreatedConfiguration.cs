﻿namespace LendFoundry.Tasks.ActionCenter.ApplicationRequestCreated
{
    public class ApplicationRequestCreatedConfiguration
    {
        public string EntityId { get; set; }

        public string EntityType { get; set; }

        public string EventName { get; set; }

        public string TemplateName { get; set; }

        public string TemplateVersion { get; set; }
    }
}