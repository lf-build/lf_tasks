﻿
using System;

namespace LendFoundry.Tasks.ActionCenter.ApplicationRequestCreated
{
    public class Settings
    {
        public const string Prefix = "TASK_APPLICATION_REQUEST_CREATED";

        public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "task_application_request_created";

        public static string Link = Environment.GetEnvironmentVariable($"{Prefix}_LINK");

        public static string EventName = Environment.GetEnvironmentVariable($"{Prefix}_EVENT_NAME");
    }
}