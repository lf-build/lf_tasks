﻿using LendFoundry.Application.Client;
using LendFoundry.Configuration.Client;
using LendFoundry.Email.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.Tasks.Agent;
using System;
using System.Linq;

namespace LendFoundry.Tasks.ActionCenter.ApplicationRequestCreated
{
    public class Agent : EventBasedAgent
    {
        private IApplicationServiceClientFactory ApplicationServiceFactory { get; }
        private ITokenReaderFactory TokenHandlerFactory { get; }
        private IEmailServiceFactory EmailServiceFactory { get; }
        private IApplicantServiceFactory ApplicantServiceFactory { get; }
        private IConfigurationServiceFactory ConfigurationFactory { get; }

        public Agent
        (
            IEventHubClientFactory factory,
            IApplicationServiceClientFactory applicationServiceFactory,
            ITokenReaderFactory tokenHandlerFactory,
            IEmailServiceFactory emailServiceFactory,
            IApplicantServiceFactory applicantServiceFactory,
            ILoggerFactory loggerFactory,
            ITokenHandler tokenHandler,
            IConfigurationServiceFactory configurationFactory = null
        ) : base(factory, Settings.ServiceName, Settings.EventName, tokenHandler, loggerFactory)
        { 
            ApplicationServiceFactory = applicationServiceFactory;
            ApplicantServiceFactory = applicantServiceFactory;
            TokenHandlerFactory = tokenHandlerFactory;
            EmailServiceFactory = emailServiceFactory;
            ApplicantServiceFactory = applicantServiceFactory;
            ConfigurationFactory = configurationFactory;
        }

        public override async void Execute(EventInfo @event)
        {
            var reader = TokenHandlerFactory.Create(@event.TenantId, string.Empty);
            var applicationService = ApplicationServiceFactory.Create(reader);
            var emailService = EmailServiceFactory.Create(reader);
            var applicantService = ApplicantServiceFactory.Create(reader);
            var logger = LoggerFactory.CreateLogger();
            var configuration = ConfigurationFactory.Create<ApplicationRequestCreatedConfiguration>(Settings.ServiceName, reader).Get();

            try
            {
                if (configuration == null)
                {
                    logger.Error($"The configuration related to {Settings.ServiceName} was not found");
                    return;
                }

                var entityId = configuration.EntityId.FormatWith(@event);
                var entityType = configuration.EntityType.FormatWith(@event);

                logger.Info($"Received request to EntityType: {entityType}, and EntityId: {entityId}");

                if (entityType.ToLower().Equals("application") || string.IsNullOrEmpty(entityId))
                {
                    var application = applicationService.GetByApplicationNumber(entityId);
                    var applicant = applicantService.Get(application.Applicants.FirstOrDefault(m => m.IsPrimary).Id);
                    var link = string.Format(Settings.Link ?? "link not available for: {0}", application.ApplicationNumber);
                    var request = new
                    {
                        TemplateName = configuration.TemplateName,
                        TemplateVersion = configuration.TemplateVersion,
                        Email = applicant.Email,
                        BorrowerName = $"{applicant.FirstName} {applicant.LastName}",
                        CustomerEmail = applicant.Email,
                        ActionLink = link
                    };

                    await emailService.Send(configuration.TemplateName, configuration.TemplateVersion, request);

                    logger.Info($"Notification sent to {applicant.Email} - {entityType}: {entityId}");
                }
            }
            catch (Exception e)
            {
                logger.Error($"Exception on event {@event.Id} - {@event.Name}", e);
            }
        }
    }
}