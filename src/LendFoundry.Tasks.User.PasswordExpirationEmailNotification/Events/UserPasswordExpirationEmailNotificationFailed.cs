﻿using LendFoundry.Security.Identity;

namespace LendFoundry.Tasks.User.PasswordExpirationEmailNotification.Events
{
    public class UserPasswordExpirationEmailNotificationFailed : UserPasswordExpirationEmailNotificationEvent
    {
    }
}