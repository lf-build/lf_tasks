﻿namespace LendFoundry.Tasks.User.PasswordExpirationEmailNotification.Events
{
    public abstract class UserPasswordExpirationEmailNotificationEvent
    {
        public string UserId { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        
    }
}