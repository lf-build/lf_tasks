﻿using System;
using System.Threading.Tasks;
using LendFoundry.Configuration;
using LendFoundry.Email.Client;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Identity.Client;
using LendFoundry.Security.Tokens;
using LendFoundry.Tasks.Agent;
using LendFoundry.Tasks.User.PasswordExpirationEmailNotification.Events;
using LendFoundry.Security.Identity;
using System.Linq;
using LendFoundry.Tenant.Client;

namespace LendFoundry.Tasks.User.PasswordExpirationEmailNotification
{
    public class Agent : ScheduledAgent
    {
        public Agent
        (
            IConfigurationServiceFactory configurationFactory,
            ITokenHandler tokenHandler,
            ITenantTimeFactory tenantTimeFactory,
            ILoggerFactory loggerFactory,
            IIdentityServiceFactory identityServiceFactory,
            IEventHubClientFactory eventhubFactory,
            IEmailServiceFactory emailServiceFactory,
            ITenantServiceFactory tenantServiceFactory
        ) : base(tokenHandler, configurationFactory, tenantTimeFactory, tenantServiceFactory, loggerFactory, Settings.ServiceName)
        {
            ConfigurationFactory = configurationFactory;
            TokenHandler = tokenHandler;
            LoggerFactory = loggerFactory;
            IdentityServiceFactory = identityServiceFactory;
            EventhubFactory = eventhubFactory;
            EmailServiceFactory = emailServiceFactory;
            TenantServiceFactory = tenantServiceFactory;
        }

        private ITenantServiceFactory TenantServiceFactory { get; }
        private IConfigurationServiceFactory ConfigurationFactory { get; }
        private ITokenHandler TokenHandler { get; }
        private ILoggerFactory LoggerFactory { get; }
        public IIdentityServiceFactory IdentityServiceFactory { get; }
        private IEmailServiceFactory EmailServiceFactory { get; }
        private IEventHubClientFactory EventhubFactory { get; }

        public override void OnSchedule(string tenant)

        {
            var logger = LoggerFactory.Create(NullLogContext.Instance);
            try
            {
                logger.Info("OnSchedule method started");

                // gets the context user
                var token = TokenHandler.Issue(tenant, Settings.ServiceName);
                var reader = new StaticTokenReader(token.Value);

                // task service dependencies
                var configurationService = ConfigurationFactory.Create<Configuration>(Settings.ServiceName, reader);
                if (configurationService == null)
                {
                    logger.Error($"Did not find the configuration related to {Settings.ServiceName}");
                    return;
                }
                var configuration = configurationService.Get();
                if (configuration == null)
                {
                    logger.Error($"The configuration related to {Settings.ServiceName} was not found");
                    return;
                }
                var configurationServiceUserSecurityIdentity = ConfigurationFactory.Create<Security.Identity.Configuration>(Settings.UserSecurityIdentityServiceName, reader);
                if (configurationServiceUserSecurityIdentity == null)
                {
                    logger.Error($"Did not find the configuration related to {Settings.ServiceName}");
                    return;
                }
                var configurationUserSecurityIdentity = configurationServiceUserSecurityIdentity.Get();
                if (configurationUserSecurityIdentity == null)
                {
                    logger.Error($"The configuration related to {Settings.UserSecurityIdentityServiceName} was not found");
                    return;
                }
                if (string.IsNullOrEmpty(configuration.TemplateName))
                {
                    logger.Error($"The configuration with template name related to {Settings.ServiceName} was not found");
                    return;
                }
                if (string.IsNullOrEmpty(configuration.TemplateVersion))
                {
                    logger.Error($"The configuration with template version related to {Settings.ServiceName} was not found");
                    return;
                }
                RoleToPermissionConverter roleUserPermission = new RoleToPermissionConverter(configurationUserSecurityIdentity);
                var eventHub = EventhubFactory.Create(reader);
                var emailService = EmailServiceFactory.Create(reader);
                
                var identityService = IdentityServiceFactory.Create(reader);
                var users = identityService.GetUsersByPasswordExpiring().Result;
                Parallel.ForEach(users, user =>
                {
                    try
                    {
                        logger.Info($"Starting to send email notification for {user.Username} with {user.Email}");

                        var linkUrl = string.Empty;

                        var roleListUser = roleUserPermission.ToPermission(user.Roles.ToList());
                        foreach (var userRole in roleListUser)
                        {
                           
                            if (configuration.PortalUrls.ContainsKey(userRole))
                            {
                                linkUrl = configuration.PortalUrls[userRole];
                                break;
                            }
                        }

                        if (string.IsNullOrWhiteSpace(linkUrl))
                            return;

                        var request = new
                        {
                            name = user.Name,
                            email = user.Email,
                            expirationDate = user.PasswordExpirationDate.GetValueOrDefault().ToString("D"),
                            linkUrl
                        };

                        var isSent = emailService.Send("user", user.Id, configuration.TemplateName,
                            configuration.TemplateVersion, request).Result;
                        if (isSent!=null && isSent.Success)
                        {
                            logger.Info($"finished to send email notification for {user.Username}");
                            eventHub.Publish(new UserPasswordExpirationEmailNotificationSent
                            {
                                UserId = user.Id,
                                Username = user.Username,
                                Email = user.Email
                            });
                        }
                        else
                        {
                            throw new Exception($"Error sending email notification for {user.Username}");
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Error("Error sending password expiration email",ex);
                        eventHub.Publish(new UserPasswordExpirationEmailNotificationFailed
                        {
                            UserId = user.Id,
                            Username = user.Username,
                            Email = user.Email
                        });
                    }
                });

            }
            catch (Exception ex)
            {
                logger.Error("Error while executing the OnSchedule method from AbandonedEmailNotification task. Error: ", ex);
            }
        }
    }
}