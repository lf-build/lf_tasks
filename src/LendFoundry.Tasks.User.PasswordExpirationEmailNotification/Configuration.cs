﻿using System.Collections.Generic;
using LendFoundry.Foundation.Client;
using LendFoundry.Security.Identity;

namespace LendFoundry.Tasks.User.PasswordExpirationEmailNotification
{
    public class Configuration:IDependencyConfiguration
    {

        public string TemplateName { get; set; }
        public string TemplateVersion { get; set; }
        public CaseInsensitiveDictionary<string> PortalUrls { get; set; }
        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }
        public string ConnectionString { get; set; }

    }
}