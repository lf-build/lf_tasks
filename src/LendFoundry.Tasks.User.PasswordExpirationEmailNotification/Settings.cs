﻿using System;


namespace LendFoundry.Tasks.User.PasswordExpirationEmailNotification
{
    public class Settings
    {
        public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "task_user_password_expiration_email_notification";
        public static string UserSecurityIdentityServiceName { get; } = "security-identity";
    }
}