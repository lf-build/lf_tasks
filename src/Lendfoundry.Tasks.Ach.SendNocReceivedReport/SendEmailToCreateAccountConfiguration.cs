﻿
using System.Collections.Generic;
using LendFoundry.Foundation.Client;

namespace Lendfoundry.Tasks.Ach.SendNocReceivedReport
{
    public class SendNocReceivedReportConfiguration : IDependencyConfiguration
    {
        public string EntityId { get; set; }

        public string EntityType { get; set; }

        public string TemplateName { get; set; }

        public string TemplateVersion { get; set; }

        public string Link { get; set; }

        public Dictionary<string,string> ToEmailAddresses { get; set; }

        public string FromEmailAddress { get; set; }

        //public string ToEmailRecieverName { get; set; }

        public Dictionary<string, string> CcEmailAddress { get; set; }
        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }
        public string ConnectionString { get; set; }
    }
}