﻿
using System;

namespace Lendfoundry.Tasks.Ach.SendNocReceivedReport
{
    public class Settings
    {
        public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "tasks_ach_send_noc_received_report";
    }
}