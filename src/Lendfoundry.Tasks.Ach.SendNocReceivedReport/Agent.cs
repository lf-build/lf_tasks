﻿using LendFoundry.Configuration;
using LendFoundry.Email.Client;
using LendFoundry.Email;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.MoneyMovement.Ach.Client;
using LendFoundry.Security.Tokens;
using LendFoundry.Tasks.Agent;
using System;
using System.Linq;
using System.Collections.Generic;
using LendFoundry.Tenant.Client;

namespace Lendfoundry.Tasks.Ach.SendNocReceivedReport
{
    public class Agent : ScheduledAgent
    {
        public Agent
          (
            IConfigurationServiceFactory configurationFactory,
            ITokenHandler tokenHandler,
            ITenantTimeFactory tenantTimeFactory,
            IAchServiceFactory achFileServiceFactory,
            ILoggerFactory loggerFactory,
            IEmailServiceFactory emailServiceFactory,
            ITenantServiceFactory tenantServiceFactory
            )
            : base(tokenHandler, configurationFactory, tenantTimeFactory, tenantServiceFactory, loggerFactory, Settings.ServiceName)
        {
            ConfigurationFactory = configurationFactory;
            TokenHandler = tokenHandler;
            TenantTimeFactory = tenantTimeFactory;
            AchFileServiceFactory = achFileServiceFactory;
            LoggerFactory = loggerFactory;
            EmailServiceFactory = emailServiceFactory;
            TenantServiceFactory = tenantServiceFactory;
        }
        private ITenantServiceFactory TenantServiceFactory { get; }
        private IConfigurationServiceFactory ConfigurationFactory { get; }

        private ITenantTimeFactory TenantTimeFactory { get; }

        private ITokenHandler TokenHandler { get; }

        private IAchServiceFactory AchFileServiceFactory { get; }

        private ILoggerFactory LoggerFactory { get; }

        private IEmailServiceFactory EmailServiceFactory { get; }

        public override async void OnSchedule(string tenant)
        {
            var token = TokenHandler.Issue(tenant, Settings.ServiceName);
            var reader = new StaticTokenReader(token.Value);
            var logger = LoggerFactory.Create(NullLogContext.Instance);
            var tenantTime = TenantTimeFactory.Create(ConfigurationFactory, reader);
            var emailService = EmailServiceFactory.Create(reader);
            var achFileService = AchFileServiceFactory.Create(reader);

            try
            {
                var configurationService = ConfigurationFactory.Create<SendNocReceivedReportConfiguration>(Settings.ServiceName, reader);
                if (configurationService == null)
                {
                    logger.Error($"Did not find the configuration related to {Settings.ServiceName}");
                    return;
                }

                var configuration = configurationService.Get();
                if (configuration == null)
                {
                    logger.Error($"The configuration related to {Settings.ServiceName} was not found");
                    return;
                }

                var nocReceivedData = achFileService.GetAchNocReceived().Result;

                //if (nocReceivedData == null || !nocReceivedData.Any())
                //{
                //    logger.Info($"No Noc received for the day");
                //    return;
                //}
                var ToAddress = new List<IEmailAddress>();
                if (configuration.ToEmailAddresses != null)
                {
                    foreach (var email in configuration.ToEmailAddresses.Keys)
                    {
                        if (!string.IsNullOrEmpty(email))
                        {
                            ToAddress.Add(new EmailAddress 
                            {
                                Email = email
                            });
                        }
                    }
                }
                var CcAddress = new List<IEmailAddress>();
                if (configuration.CcEmailAddress != null)
                {
                    foreach (var email in configuration.CcEmailAddress.Keys)
                    {
                        if (!string.IsNullOrEmpty(email))
                        {
                            CcAddress.Add(new EmailAddress
                            {
                                Email = email
                            });
                        }
                    }
                }
                var request = new
                {
                    Data = nocReceivedData,
                    email = ToAddress.First().Email
                };

                await emailService.Send(configuration.TemplateName, configuration.TemplateVersion, request);

                logger.Info($"Notification sent to {ToAddress.First().Email}");
            }
            catch (Exception ex)
            {
                logger.Error($"Exception while sending notification", ex);
            }
        }
    }
}