using System;

namespace LendFoundry.Tasks.Ach.CreateAchFiles
{
    public static class Settings
    {
        public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "task_create_ach_files";
    }
}