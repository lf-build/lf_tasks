﻿using LendFoundry.Calendar.Client;
using LendFoundry.Configuration.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.MoneyMovement.Ach.Client;
using LendFoundry.MoneyMovement.Ach.Configuration;
using LendFoundry.Security.Tokens;
using LendFoundry.Foundation.ServiceDependencyResolver;
#if DOTNET2
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.AspNet.Http;
using Microsoft.Framework.DependencyInjection;
#endif
using System;
using LendFoundry.Tenant.Client;

namespace LendFoundry.Tasks.Ach.CreateAchFiles
{
    public class Program : DependencyInjection
    {
        public static void Main(string[] args)
        {

            Console.WriteLine("Application started");

            Program p = new Program();
            p.Provider.GetService<IAgent>().Execute();
            Console.WriteLine("Application terminated");
           
        }

        protected override IServiceCollection ConfigureServices(IServiceCollection services)
        {
            services.AddTokenHandler();
            services.AddTenantTime();
            services.AddTenantService();
            services.AddServiceLogging(Settings.ServiceName, NullLogContext.Instance);
            services.AddCalendarService();
            services.AddAchService();
            services.AddConfigurationService<AchConfiguration>(Settings.ServiceName);
            services.AddDependencyServiceUriResolver<AchConfiguration>(Settings.ServiceName);
            services.AddTransient<IHttpContextAccessor, EmptyHttpContextAccessor>();
            services.AddTransient<IAgent, Agent>();

            return services;
        }
    }
}