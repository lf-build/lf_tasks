using LendFoundry.Calendar.Client;
using LendFoundry.Configuration;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.MoneyMovement.Ach;
using LendFoundry.MoneyMovement.Ach.Client;
using LendFoundry.MoneyMovement.Ach.Configuration;
using LendFoundry.Security.Tokens;
using LendFoundry.Tasks.Agent;
using LendFoundry.Tenant.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Tasks.Ach.CreateAchFiles
{
    public class Agent : ScheduledAgent
    {
        public Agent
        (
            IConfigurationServiceFactory configurationFactory, 
            ITokenHandler tokenHandler,
            ITenantTimeFactory tenantTimeFactory, 
            ICalendarServiceFactory calendarServiceFactory, 
            IAchServiceFactory achServiceFactory, 
            ILoggerFactory loggerFactory,
            ITenantServiceFactory tenantServiceFactory)
            : base(tokenHandler, configurationFactory, tenantTimeFactory, tenantServiceFactory, loggerFactory, Settings.ServiceName)
        {
            ConfigurationFactory = configurationFactory;
            TokenHandler = tokenHandler;
            TenantTimeFactory = tenantTimeFactory;
            CalendarServiceFactory = calendarServiceFactory;
            AchServiceFactory = achServiceFactory;
            LoggerFactory = loggerFactory;
            TenantServiceFactory = tenantServiceFactory;
        }

        private ITenantServiceFactory TenantServiceFactory { get; }
        private IConfigurationServiceFactory ConfigurationFactory { get; }

        private ITenantTimeFactory TenantTimeFactory { get; }

        private ITokenHandler TokenHandler { get; }

        private ICalendarServiceFactory CalendarServiceFactory { get; }

        private IAchServiceFactory AchServiceFactory { get; }

        private ILoggerFactory LoggerFactory { get; }

        public override void OnSchedule(string tenant)
        {
            var token = TokenHandler.Issue(tenant, Settings.ServiceName);
            var reader = new StaticTokenReader(token.Value);
            var logger = LoggerFactory.Create(NullLogContext.Instance);

            var tenantTime = TenantTimeFactory.Create(ConfigurationFactory, reader);
            var calendar = CalendarServiceFactory.Create(reader);
            var ach = AchServiceFactory.Create(reader);

            if (!IsBusinessDay(calendar, tenantTime))
            {
                logger.Warn("Not a business day");
                return;
            }

            var configuration = ConfigurationFactory.Create<AchConfiguration>("ach", reader);
            if (configuration == null)
            {
                logger.Error("Was not found the Configuration related to CreateAchFile Task");
                return;
            }

            var achConfiguration = configuration.Get();
            if (achConfiguration == null)
            {
                logger.Error("Was not found the Configuration related to ach");
                return;
            }

            var fundingSources = achConfiguration.FundingSources;
            if (fundingSources == null || !fundingSources.Any())
            {
                logger.Warn("Was not found any fundingSources.");
                return;
            }
            
            logger.Info("Creating ACH file");
            Parallel.ForEach(fundingSources, fundingSource =>
            {
                try
                {
                    var files = ach.Create(fundingSource.Id, null) ?? new List<IAchFile>();
                    logger.Info($"FundingSource: {fundingSource.Id} | Total Files: {files.Count} was created");
                }
                catch (Exception ex)
                {
                    logger.Error($"Failed attempt to create a Ach File. Funding: {fundingSource.Id}\n The Ach is returning this error:", ex);
                }
            });
        }

        private static bool IsBusinessDay(ICalendarService calendar, ITenantTime tenantTime)
        {
            var today = tenantTime.Today;
            var todayInfo = calendar.GetDate(today.Year, today.Month, today.Day);
            return todayInfo.Type == DateType.Business;
        }
    }
}