﻿
using System;

namespace LendFoundry.Tasks.DecisionEngine.Schedule
{
    public static class Settings
    {
        public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "task-decision-engine-schedule";

        public static string Tenant { get; } = Environment.GetEnvironmentVariable("TASK_TENANT");

        public static string Schedule { get; } = Environment.GetEnvironmentVariable("TASK_SCHEDULE") ?? "";

        public static string FunctionName { get; } = Environment.GetEnvironmentVariable($"TASK_FUNCTION_NAME");
    }
}