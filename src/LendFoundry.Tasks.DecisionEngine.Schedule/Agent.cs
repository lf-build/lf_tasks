﻿using LendFoundry.Configuration;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using LendFoundry.Tasks.Agent;
using System;
using LendFoundry.Tenant.Client;

namespace LendFoundry.Tasks.DecisionEngine.Schedule
{
    public class Agent : ScheduledAgent
    {
        public Agent
        (
            IConfigurationServiceFactory configurationFactory,
            ITokenHandler tokenHandler,
            ITenantTimeFactory tenantTimeFactory,
            ITokenReaderFactory tokenReaderFactory,
            ILoggerFactory loggerFactory,
            IDecisionEngineClientFactory decisionEngineFactory,
            ITenantServiceFactory tenantServiceFactory
        )
            : base(tokenHandler, configurationFactory, tenantTimeFactory, tenantServiceFactory, loggerFactory, Settings.ServiceName)
        {
            TokenHandler = tokenHandler;
            TokenReaderFactory = tokenReaderFactory;
            DecisionEngineFactory = decisionEngineFactory;
            FunctionName = Settings.FunctionName;
            LoggerFactory = loggerFactory;
            TenantServiceFactory = tenantServiceFactory;
        }

        private ITenantServiceFactory TenantServiceFactory { get; }
        private ITokenHandler TokenHandler { get; }

        private ITokenReaderFactory TokenReaderFactory { get; }

        private IDecisionEngineClientFactory DecisionEngineFactory { get; set; }
        
        private ILoggerFactory LoggerFactory { get; }

        private string FunctionName { get; set; }

        public override void OnSchedule(string tenant)
        {
            var logger = LoggerFactory.CreateLogger();
            logger.Info($"Started executing function {FunctionName}.");
            try
            {
                var token = TokenHandler.Issue(tenant, Settings.ServiceName);
                var reader = TokenReaderFactory.Create(tenant, string.Empty);
                var decisionEngine = DecisionEngineFactory.Create(reader);
                decisionEngine.Execute<dynamic>(FunctionName, new {});
                logger.Info($"Function {FunctionName} executed.");
            }
            catch (Exception ex)
            {
                logger.Error($"Problems executing function {FunctionName}", ex);
            }
        }
    }
}