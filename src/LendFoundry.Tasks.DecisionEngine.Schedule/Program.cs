﻿using LendFoundry.Clients.DecisionEngine;
using LendFoundry.Configuration.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.Foundation.ServiceDependencyResolver;
using System.Collections.Generic;
using LendFoundry.Foundation.Client;
using LendFoundry.Tenant.Client;
#if DOTNET2
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.AspNet.Http;
using Microsoft.Framework.DependencyInjection;
#endif
using System;

namespace LendFoundry.Tasks.DecisionEngine.Schedule
{
    public class Program : DependencyInjection
    {

        public static void Main()
        {
            Console.WriteLine("Application started");

            Program p = new Program();
            p.Provider.GetService<IAgent>().Execute();
            Console.WriteLine("Application terminated");
        }


        protected override IServiceCollection ConfigureServices(IServiceCollection services)
        {
            services.AddTokenHandler();
            services.AddTenantTime();
            services.AddTransient<IHttpContextAccessor, EmptyHttpContextAccessor>();
            services.AddServiceLogging(Settings.ServiceName, NullLogContext.Instance);
            services.AddDecisionEngine();
            services.AddTenantService();
            services.AddConfigurationService<EmptyConfiguration>(Settings.ServiceName);
            services.AddDependencyServiceUriResolver<EmptyConfiguration>(Settings.ServiceName);         
            services.AddTransient<IAgent, Agent>();
            return services;
        }

        private class EmptyConfiguration : IDependencyConfiguration
        {
            public Dictionary<string, string> Dependencies { get; set; }
            public string Database { get; set; }
            public string ConnectionString { get; set; }
        }
    }
}
