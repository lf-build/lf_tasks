using System;


namespace LendFoundry.Tasks.Ach.SendAchFiles
{
    public static class Settings
    {
        public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "task-send-ach-files";
    }
}