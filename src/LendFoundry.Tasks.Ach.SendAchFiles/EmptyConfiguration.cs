using System.Collections.Generic;
using LendFoundry.Foundation.Client;

namespace LendFoundry.Tasks.Ach.SendAchFiles
{
    public class EmptyConfiguration: IDependencyConfiguration
    {
        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }
        public string ConnectionString { get; set; }
    }
}