﻿using LendFoundry.Calendar.Client;
using LendFoundry.Configuration.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.MoneyMovement.Ach;
using LendFoundry.MoneyMovement.Ach.Configuration;
using LendFoundry.Security.Tokens;
using LendFoundry.Tasks.Tests.Common;
using Moq;
using System;
using System.Collections.Generic;
using Xunit;

namespace LendFoundry.Tasks.Ach.SendAchFiles.Tests
{
    public class SendAchFilesAgentTests : InMemoryObjects
    {
        public SendAchFilesAgentTests()
        {
            ConfigurationFactory.Setup(x => x.Create<AchConfiguration>(It.IsAny<string>(), It.IsAny<ITokenReader>()))
                .Returns(ConfigurationService.Object);

            TokenHandler.Setup(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Mock.Of<IToken>());

            TenantTimeFactory.Setup(x => x.Create(It.IsAny<IConfigurationServiceFactory>(), It.IsAny<ITokenReader>()))
                .Returns(TenantTime.Object);

            CalendarServiceFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
                .Returns(CalendarService.Object);

            AchServiceFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()))
                .Returns(AchService.Object);

            Logger.Setup(x => x.Warn(It.IsAny<string>()));
            LoggerFactory.Setup(x => x.Create(It.IsAny<ILogContext>()))
                .Returns(Logger.Object);
        }

        private Agent Agent => new Agent
        (
            ConfigurationFactory.Object,
            TokenHandler.Object,
            TenantTimeFactory.Object,
            CalendarServiceFactory.Object,
            AchServiceFactory.Object,
            LoggerFactory.Object
        );

        private Mock<ISettingReader> SettingReader { get; } = new Mock<ISettingReader>();

        [Fact]
        public void OnSchedule_Test()
        {
            var tenant = "TASK_TENANT";
            var schedule = "TASK_SCHEDULE";

            SettingReader.Setup(x => x.GetSetting(tenant)).Returns("my-tenant");
            SettingReader.Setup(x => x.GetSetting(schedule)).Returns("0 0/1 * 1/1 * ? *");

            TenantTime.Setup(x => x.Today)
                .Returns(new DateTimeOffset(DateTime.Now));

            CalendarService.Setup(x => x.GetDate(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>()))
                .Returns(new DateInfo { Type = DateType.Business });

            ConfigurationService.Setup(x => x.Get())
                .Returns(new AchConfiguration
                {
                    FundingSources = new List<FundingSource>
                    {
                        new FundingSource { Id = "FundingSourceId" }
                    }
                });

            AchService.Setup(x => x.Get(It.IsAny<string>(), It.IsAny<DateTime?>()))
                .Returns(new List<IAchFile>
                {
                    new AchFile { Id = "AchFileId" }
                });

            Logger.Setup(x => x.Info(It.IsAny<string>()));
            Logger.Setup(x => x.Warn(It.IsAny<string>()));
            Logger.Setup(x => x.Error(It.IsAny<string>()));

            Agent.OnSchedule();

            // Assert
            TokenHandler.Verify(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeastOnce);
            TenantTimeFactory.Verify(x => x.Create(It.IsAny<IConfigurationServiceFactory>(), It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            CalendarServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()));
            AchServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            LoggerFactory.Verify(x => x.Create(It.IsAny<ILogContext>()), Times.AtLeastOnce);
            ConfigurationFactory.Verify(x => x.Create<AchConfiguration>(It.IsAny<string>(), It.IsAny<ITokenReader>()), Times.AtLeastOnce);

            TenantTime.Verify(x => x.Today);
            CalendarService.Verify(x => x.GetDate(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>()), Times.AtLeastOnce);
            ConfigurationService.Verify(x => x.Get(), Times.AtLeastOnce);
            AchService.Verify(x => x.Get(It.IsAny<string>(), It.IsAny<DateTime?>()), Times.AtLeastOnce);

            Logger.Verify(x => x.Info(It.IsAny<string>()), Times.AtLeast(3));
            Logger.Verify(x => x.Warn(It.IsAny<string>()), Times.Never);
            Logger.Verify(x => x.Error(It.IsAny<string>()), Times.Never);
        }

        [Fact]
        public void OnSchedule_When_IsNotBusinessDay()
        {
            var tenant = "TASK_TENANT";
            var schedule = "TASK_SCHEDULE";

            SettingReader.Setup(x => x.GetSetting(tenant)).Returns("my-tenant");
            SettingReader.Setup(x => x.GetSetting(schedule)).Returns("0 0/1 * 1/1 * ? *");

            TenantTime.Setup(x => x.Today)
                .Returns(new DateTimeOffset(DateTime.Now));

            CalendarService.Setup(x => x.GetDate(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>()))
                .Returns(new DateInfo { Type = DateType.Holiday });

            Logger.Setup(x => x.Info(It.IsAny<string>()));
            Logger.Setup(x => x.Warn(It.IsAny<string>()));

            Agent.OnSchedule();

            // Assert
            TokenHandler.Verify(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeastOnce);
            TenantTimeFactory.Verify(x => x.Create(It.IsAny<IConfigurationServiceFactory>(), It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            CalendarServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()));
            AchServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            LoggerFactory.Verify(x => x.Create(It.IsAny<ILogContext>()), Times.AtLeastOnce);
            
            TenantTime.Verify(x => x.Today);
            CalendarService.Verify(x => x.GetDate(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>()), Times.AtLeastOnce);
            Logger.Verify(x => x.Info(It.IsAny<string>()), Times.Never);
            Logger.Verify(x => x.Warn(It.IsAny<string>()), Times.AtLeastOnce);
        }

        [Fact]
        public void OnSchedule_When_HasNoFound_Configuration()
        {
            var tenant = "TASK_TENANT";
            var schedule = "TASK_SCHEDULE";

            SettingReader.Setup(x => x.GetSetting(tenant)).Returns("my-tenant");
            SettingReader.Setup(x => x.GetSetting(schedule)).Returns("0 0/1 * 1/1 * ? *");

            TenantTime.Setup(x => x.Today)
                .Returns(new DateTimeOffset(DateTime.Now));

            CalendarService.Setup(x => x.GetDate(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>()))
                .Returns(new DateInfo { Type = DateType.Business });

            ConfigurationFactory.Setup(x => x.Create<AchConfiguration>(It.IsAny<string>(), It.IsAny<ITokenReader>()));

            Logger.Setup(x => x.Info(It.IsAny<string>()));
            Logger.Setup(x => x.Warn(It.IsAny<string>()));
            Logger.Setup(x => x.Error(It.IsAny<string>()));

            Agent.OnSchedule();

            // Assert
            TokenHandler.Verify(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeastOnce);
            TenantTimeFactory.Verify(x => x.Create(It.IsAny<IConfigurationServiceFactory>(), It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            CalendarServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()));
            AchServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            LoggerFactory.Verify(x => x.Create(It.IsAny<ILogContext>()), Times.AtLeastOnce);
            ConfigurationFactory.Verify(x => x.Create<AchConfiguration>(It.IsAny<string>(), It.IsAny<ITokenReader>()), Times.AtLeastOnce);

            TenantTime.Verify(x => x.Today);
            CalendarService.Verify(x => x.GetDate(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>()), Times.AtLeastOnce);
            
            Logger.Verify(x => x.Info(It.IsAny<string>()), Times.Never);
            Logger.Verify(x => x.Warn(It.IsAny<string>()), Times.Never);
            Logger.Verify(x => x.Error(It.IsAny<string>()), Times.AtLeastOnce);
        }

        [Fact]
        public void OnSchedule_When_HasNoFound_AchConfiguration()
        {
            var tenant = "TASK_TENANT";
            var schedule = "TASK_SCHEDULE";

            SettingReader.Setup(x => x.GetSetting(tenant)).Returns("my-tenant");
            SettingReader.Setup(x => x.GetSetting(schedule)).Returns("0 0/1 * 1/1 * ? *");

            TenantTime.Setup(x => x.Today)
                .Returns(new DateTimeOffset(DateTime.Now));

            CalendarService.Setup(x => x.GetDate(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>()))
                .Returns(new DateInfo { Type = DateType.Business });

            ConfigurationService.Setup(x => x.Get());

            Logger.Setup(x => x.Info(It.IsAny<string>()));
            Logger.Setup(x => x.Warn(It.IsAny<string>()));
            Logger.Setup(x => x.Error(It.IsAny<string>()));

            Agent.OnSchedule();

            // Assert
            TokenHandler.Verify(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeastOnce);
            TenantTimeFactory.Verify(x => x.Create(It.IsAny<IConfigurationServiceFactory>(), It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            CalendarServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()));
            AchServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            LoggerFactory.Verify(x => x.Create(It.IsAny<ILogContext>()), Times.AtLeastOnce);
            ConfigurationFactory.Verify(x => x.Create<AchConfiguration>(It.IsAny<string>(), It.IsAny<ITokenReader>()), Times.AtLeastOnce);

            TenantTime.Verify(x => x.Today);
            CalendarService.Verify(x => x.GetDate(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>()), Times.AtLeastOnce);
            ConfigurationService.Verify(x => x.Get(), Times.AtLeastOnce);

            Logger.Verify(x => x.Info(It.IsAny<string>()), Times.Never);
            Logger.Verify(x => x.Warn(It.IsAny<string>()), Times.Never);
            Logger.Verify(x => x.Error(It.IsAny<string>()), Times.AtLeastOnce);
        }

        [Fact]
        public void OnSchedule_When_HasNoFound_FundingSources()
        {
            var tenant = "TASK_TENANT";
            var schedule = "TASK_SCHEDULE";

            SettingReader.Setup(x => x.GetSetting(tenant)).Returns("my-tenant");
            SettingReader.Setup(x => x.GetSetting(schedule)).Returns("0 0/1 * 1/1 * ? *");

            TenantTime.Setup(x => x.Today)
                .Returns(new DateTimeOffset(DateTime.Now));

            CalendarService.Setup(x => x.GetDate(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>()))
                .Returns(new DateInfo { Type = DateType.Business });

            ConfigurationService.Setup(x => x.Get())
                .Returns(new AchConfiguration
                {
                    FundingSources = new List<FundingSource>()
                });

            Logger.Setup(x => x.Info(It.IsAny<string>()));
            Logger.Setup(x => x.Warn(It.IsAny<string>()));
            Logger.Setup(x => x.Error(It.IsAny<string>()));

            Agent.OnSchedule();

            // Assert
            TokenHandler.Verify(x => x.Issue(It.IsAny<string>(), It.IsAny<string>()), Times.AtLeastOnce);
            TenantTimeFactory.Verify(x => x.Create(It.IsAny<IConfigurationServiceFactory>(), It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            CalendarServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()));
            AchServiceFactory.Verify(x => x.Create(It.IsAny<ITokenReader>()), Times.AtLeastOnce);
            LoggerFactory.Verify(x => x.Create(It.IsAny<ILogContext>()), Times.AtLeastOnce);
            ConfigurationFactory.Verify(x => x.Create<AchConfiguration>(It.IsAny<string>(), It.IsAny<ITokenReader>()), Times.AtLeastOnce);

            TenantTime.Verify(x => x.Today);
            CalendarService.Verify(x => x.GetDate(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>()), Times.AtLeastOnce);
            ConfigurationService.Verify(x => x.Get(), Times.AtLeastOnce);

            Logger.Verify(x => x.Info(It.IsAny<string>()), Times.AtLeastOnce);
            Logger.Verify(x => x.Warn(It.IsAny<string>()), Times.Never);
            Logger.Verify(x => x.Error(It.IsAny<string>()), Times.Never);
        }


        [Fact]
        public void Program_Test()
        {
            var program = new Program();

            Assert.NotNull(program);

            //program.Main(null);
        }
    }
}