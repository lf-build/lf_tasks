﻿using LendFoundry.Applicant;
using LendFoundry.Application;
using LendFoundry.Application.Client;
using LendFoundry.Configuration.Client;
using LendFoundry.Email;
using LendFoundry.Email.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using LendFoundry.Tasks.ActionCenter.ApplicationRequestCreated;
using Moq;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using Xunit;

namespace LendFoundry.Tasks.ActionCenter.Application.Test
{
    public class AgentTest
    {
        private Mock<IEventHubClientFactory> EventHubClientFactory { get; } = new Mock<IEventHubClientFactory>();
        private Mock<ITokenReaderFactory> TokenHandlerFactory { get; } = new Mock<ITokenReaderFactory>();
        private Mock<IApplicationServiceClientFactory> ApplicationServiceFactory { get; } = new Mock<IApplicationServiceClientFactory>();
        private Mock<IEmailServiceFactory> EmailServiceFactory { get; } = new Mock<IEmailServiceFactory>();
        private Mock<IApplicantServiceFactory> ApplicantServiceFactory { get; } = new Mock<IApplicantServiceFactory>();
        private Mock<ILoggerFactory> LoggerFactory { get; } = new Mock<ILoggerFactory>();
        private Mock<IConfigurationServiceFactory> ConfigurationServiceFactory { get; } = new Mock<IConfigurationServiceFactory>();
        private Mock<ITokenReader> TokenReader { get; } = new Mock<ITokenReader>();
        private Mock<ITokenHandler> TokenHandler { get; } = new Mock<ITokenHandler>();
        private Mock<IApplicationService> ApplicationService { get; } = new Mock<IApplicationService>();
        private Mock<IEmailService> EmailService { get; } = new Mock<IEmailService>();
        private Mock<IApplicantService> ApplicantService { get; } = new Mock<IApplicantService>();
        private Mock<ILogger> Logger { get; } = new Mock<ILogger>();
        private Mock<ITenantTimeFactory> TenantTimeFactory { get; } = new Mock<ITenantTimeFactory>(); 
        private Mock<IConfigurationService<ApplicationRequestCreatedConfiguration>> ConfigurationService { get; } = new Mock<IConfigurationService<ApplicationRequestCreatedConfiguration>>();
        
        private Mock<IApplicant> Applicant { get; } = new Mock<IApplicant>();
        private Mock<IApplicationResponse> Application { get; } = new Mock<IApplicationResponse>();
        private Mock<LendFoundry.Application.IApplicantRequest> ApplicantRequest { get; } = new Mock<LendFoundry.Application.IApplicantRequest>();
        private Mock<ApplicationRequestCreatedConfiguration> ApplicationRequestCreatedConfiguration { get; } = new Mock<ApplicationRequestCreatedConfiguration>();

        private EventInfo @event = new EventInfo()
        {
            TenantId = "my-merchant",
            Name = "ApplicationRequestCreated",
            Data = JObject.FromObject(new
            {

                    Request = new
                    {
                        EntityType = "Application",
                        EntityId = "000118"
                    }
                
            })
        };

        private void SetupFactoryMocks()
        {
            // setup token reader factory
            TokenHandlerFactory.Setup(m => m.Create(It.IsAny<string>(), It.IsAny<string>())).Returns(TokenReader.Object);

            // setup application service factory
            ApplicationServiceFactory.Setup(m => m.Create(TokenReader.Object)).Returns(ApplicationService.Object);

            // setup applicant service factory
            ApplicantServiceFactory.Setup(m => m.Create(TokenReader.Object)).Returns(ApplicantService.Object);

            // setup email service factory
            EmailServiceFactory.Setup(m => m.Create(TokenReader.Object)).Returns(EmailService.Object);

            //setup log service factory
            LoggerFactory.Setup(m => m.CreateLogger()).Returns(Logger.Object);

            //setup configuration service factory
            ConfigurationServiceFactory.Setup(m => m.Create<ApplicationRequestCreatedConfiguration>(It.IsAny<string>(), TokenReader.Object)).Returns(ConfigurationService.Object);

        }

        private ApplicationRequestCreatedConfiguration AppRequestCreatedConfig { get; } =
            new ApplicationRequestCreated.ApplicationRequestCreatedConfiguration
            {
                EntityId = "{Data.Request.EntityId}",
                EntityType = "{Data.Request.EntityType}",
                EventName = "EventName",
                TemplateName = "TemplateName",
                TemplateVersion = "TemplateVersion"
            };

        private IApplicant ApplicantFake { get; } = new Applicant.Applicant
        {
            FirstName = "FirstName",
            LastName = "LastName",
            Email = "Email"
        };

        private LendFoundry.Application.IApplicantRequest ApplicantRequestFake { get; } 
            = new LendFoundry.Application.ApplicantRequest
        {
            Id = "ApplicantId",
            IsPrimary = true
        };

        private LendFoundry.Application.IApplicationResponse ApplicationFake { get; } = new LendFoundry.Application.ApplicationResponse
        {
            Applicants = new List<LendFoundry.Application.IApplicantRequest>
            (
                new LendFoundry.Application.IApplicantRequest[] 
                {
                    new LendFoundry.Application.ApplicantRequest
                    {
                        Id = "ApplicantId",
                        IsPrimary = true
                    }
                }
            )
        };

        private void SetupResponseMocks()
        {
            ApplicationRequestCreatedConfiguration.SetupProperty(c => c.EntityId).SetupGet(c => c.EntityId).Returns("{Data.Request.EntityId}");
            ApplicationRequestCreatedConfiguration.SetupGet(c => c.EntityType).Returns("{Data.Request.EntityType}");
            ApplicationRequestCreatedConfiguration.SetupGet(c => c.EventName).Returns("EventName");
            ApplicationRequestCreatedConfiguration.SetupGet(c => c.TemplateName).Returns("TemplateName");
            ApplicationRequestCreatedConfiguration.SetupGet(c => c.TemplateVersion).Returns("TemplateVersion");

            Applicant.SetupGet(a => a.FirstName).Returns("FirstName");
            Applicant.SetupGet(a => a.LastName).Returns("LastName");
            Applicant.SetupGet(a => a.Email).Returns("Email");

            ApplicantRequest.SetupGet(a => a.Id).Returns("ApplicantId");
            ApplicantRequest.SetupGet(a => a.IsPrimary).Returns(true);

            Application.SetupGet(a => a.Applicants)
                .Returns(new List<LendFoundry.Application.IApplicantRequest>(new 
                LendFoundry.Application.IApplicantRequest[] { ApplicantRequest.Object }));
        }


        private ApplicationRequestCreated.Agent BuildAgent()
        {
            return new ApplicationRequestCreated.Agent
                (
                EventHubClientFactory.Object, 
                ApplicationServiceFactory.Object,
                TokenHandlerFactory.Object, 
                EmailServiceFactory.Object, 
                ApplicantServiceFactory.Object,
                LoggerFactory.Object,
                TokenHandler.Object,
                ConfigurationServiceFactory.Object
                );
        }

        [Fact]
        public void ApplicationRequestCreatedNotification()
        {
            //given
            SetupFactoryMocks();
            //SetupResponseMocks();

            ApplicationService.Setup(x => x.GetByApplicationNumber(It.IsAny<string>())).Returns(ApplicationFake);
            ApplicantService.Setup(x => x.Get(It.IsAny<string>())).Returns(ApplicantFake);
            ApplicantService.Setup(x => x.CreateIdentityToken(It.IsAny<string>())).Returns("token");
            ConfigurationService.Setup(s => s.Get()).Returns(AppRequestCreatedConfig);

            var tenantTime = new Mock<ITenantTime>();
            tenantTime.Setup(x => x.Now).Returns(new System.DateTimeOffset(System.DateTime.Now));
            TenantTimeFactory.Setup(x => x.Create(It.IsAny<IConfigurationServiceFactory>(), It.IsAny<ITokenReader>())).
                Returns(tenantTime.Object);

            var token = new Mock<IToken>();
            token.Setup(x => x.Value).Returns("Token");

            TokenHandler.Setup(x => x.Issue(It.IsAny<string>(), It.IsAny<string>(), null, It.IsAny<string>(), It.IsAny<string[]>())).
                Returns(token.Object);

            var agent = BuildAgent();

            //when
            agent.Execute(@event);

            //then
            ApplicationServiceFactory.Verify(m => m.Create(TokenReader.Object), Times.Once);
            ApplicantServiceFactory.Verify(m => m.Create(TokenReader.Object), Times.Once);
            EmailServiceFactory.Verify(m => m.Create(TokenReader.Object), Times.Once);
            LoggerFactory.Verify(m => m.CreateLogger(), Times.Once);

            ApplicationService.Verify(x => x.GetByApplicationNumber(It.IsAny<string>()), Times.Once);
            ApplicantService.Verify(x => x.Get(It.IsAny<string>()), Times.Once);
            Logger.Verify(l => l.Info(It.IsAny<string>()), Times.AtLeastOnce);
        }

        [Fact]
        public void ApplicationRequestCreatedNotificationWithADiferentEntityType()
        {
            //given
            SetupFactoryMocks();
            //SetupResponseMocks();

            @event.Data = JObject.FromObject(new
            {
                EntityType = "Another",
                EntityId = "xyz"
            });

            ApplicationService.Setup(x => x.GetByApplicationNumber(It.IsAny<string>())).Returns(ApplicationFake);
            ApplicantService.Setup(x => x.Get(It.IsAny<string>())).Returns(ApplicantFake);
            ApplicantService.Setup(x => x.CreateIdentityToken(It.IsAny<string>())).Returns("token");
            ConfigurationService.Setup(s => s.Get()).Returns(AppRequestCreatedConfig);

            ConfigurationService.Setup(s => s.Get()).Returns(new ApplicationRequestCreatedConfiguration());

            var agent = BuildAgent();

            //when
            agent.Execute(@event);

            //then
            ApplicationServiceFactory.Verify(m => m.Create(TokenReader.Object), Times.Once);
            ApplicantServiceFactory.Verify(m => m.Create(TokenReader.Object), Times.Once);
            EmailServiceFactory.Verify(m => m.Create(TokenReader.Object), Times.Once);
            LoggerFactory.Verify(m => m.CreateLogger(), Times.Once);
            
            ApplicationService.Verify(x => x.GetByApplicationNumber(It.IsAny<string>()), Times.Never);
            ApplicantService.Verify(x => x.Get(It.IsAny<string>()), Times.Never);
        }
    }
}